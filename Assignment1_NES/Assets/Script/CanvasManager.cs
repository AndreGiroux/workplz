﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

using UnityEngine.UI;


public class CanvasManager : MonoBehaviour {



    public Button startButton;
    public Button quitButton;
    public Button titleButton;

    public Button resumeButton;


    // Use this for initialization
    void Start () {

        if (!GameManager.instance)
        {
            Debug.LogError("MISSING!");
        }


        if (GameManager.instance)
        {
            if (startButton)
            {
                startButton.onClick.AddListener(GameManager.instance.StartGame);
            }


            if (titleButton)
            {
                startButton.onClick.AddListener(GameManager.instance.TitleScreen);
                // change scene
            }



            if (resumeButton)
            {

                Debug.Log("Resumed press");
                resumeButton.onClick.AddListener(GameManager.instance.ResumeGame);


            }

            if (quitButton)
            {
                Debug.Log("Quitting game..");
                quitButton.onClick.AddListener(GameManager.instance.QuitGame);
            }















        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
