﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public int lives;

    //pause panel
    public GameObject pausePanel;


    // delete excess, dont destroy unnecessery ones.
    static GameManager _instance = null;


    //public player prefab
    public GameObject playerPrefab;

    // is it paused?
    public bool paused = false;


    // score
    int _score;
    public Text scoreText;


    // Use this for initialization
    void Start()
    {

        // lives
        if (lives <= 0)
        {
            lives = 3;
            Debug.Log("lives set to 3");
        }

        // nothing
        //keeping scene alive when its switched


        // spawn character
        if (instance)
        {
            //if it exists, destroy it!
            Destroy(gameObject);
        }


        else
        {
            // set and don't destroy
            instance = this;
            DontDestroyOnLoad(gameObject);
        }



    }

    // Update is called once per frame
    void Update()
    {

        if (lives == 2)
        {
            Debug.Log("Lives = 2");
        }

        if (lives == 1)
        {
            Debug.Log("Lives = 1");
        }





        if (lives <= 0)
        {
            // load gameover scene
            SceneManager.LoadScene("GameOver");



            //reset lives
            lives = 3;
            //Debug.Log();

        }




        //if escape has been pressed, switch scenes

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            // check to see which scene is active

            // go to level 1
            if (SceneManager.GetActiveScene().name == "Title")
            {
                // swithc scenes
                SceneManager.LoadScene("Level1");
            }

            // go back to title screen
            else if (SceneManager.GetActiveScene().name == "Level1")
            {
                // swithc scenes
                SceneManager.LoadScene("Title");
            }

            else if (SceneManager.GetActiveScene().name == "GameOver")
            {
                // swithc scenes
                SceneManager.LoadScene("Title");
            }

        }







    }




    //spawn
    public void spawnPlayer(int spawnLocation)
    //public void spawnPlayer(Transform spawnLocation)
    //public void spawnPlayer(GameObject spawnLocation)
    {
        Debug.Log("it entered");

        string spawnPoint = SceneManager.GetActiveScene().name + "_" + spawnLocation;

        //where is it
        Transform spawnPointTransform = GameObject.Find(spawnPoint).GetComponent<Transform>();

        Instantiate(playerPrefab, spawnPointTransform.position, spawnPointTransform.rotation);
    }



  



    // BUTTONS

    public void ResumeGame()
    {
        Debug.Log("Resume game..");
        paused = false;
        pausePanel.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    // only works in exe, not in the unity editor
    public void QuitGame()
    {
        Debug.Log("Quitting game..");
        Application.Quit();
    }


    public void StartGame()
    {
        Debug.Log("Start has been called");
        SceneManager.LoadScene("Level1");
    }



    public void TitleScreen()
    {
        SceneManager.LoadScene("Title");
    }



      // get set
    public static GameManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }



    // UI
    public int score
    {
        get { return _score; }
        set
        {
            _score = value;

            if (scoreText)
            {
                scoreText.text = "Score: " + _score;
            }
        }
    }




}
