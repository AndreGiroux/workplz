﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannon : MonoBehaviour {


    public float speed;

    public bool playerLeft;
    
    // projectile instantiation
    public Transform projectileSpawnRight;
    public Transform projectileSpawnLeft;

    // name of the script of the bullet
    public projectileBullet projectileBulletPrefab;

    public float projectileForce;

    // playerdetector

    public bool detectPlayer;

    public float fireRate;
    float timeLastFire;


    public GameObject target = null;

    // Use this for initialization
    void Start () {

        if (!projectileSpawnRight)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No projectileSpawnRight found on GameObject");
        }

        if (!projectileSpawnLeft)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No projectileSpawnLeft found on GameObject");
        }


        if (!projectileBulletPrefab)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No projectilePrefab found on GameObject");
        }


        if (projectileForce == 0)
        {
            // Assign a default value if one is not set in the Inspector
            projectileForce = 7.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("projectileForce was not set. Defaulting to " + projectileForce);
        }


        // Check if speed was set to something not 0
        if (fireRate == 0)
        {
            // Assign a default value if one is not set in the Inspector
            fireRate = 2.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("projectileForce was not set. Defaulting to " + projectileForce);
        }

        timeLastFire = 0.0f;




        // initialize values of direction
        if (!target)
            target = GameObject.FindWithTag("Player");



    }
	
	// Update is called once per frame
	void Update () {




        if (detectPlayer == true)
        {
            if (Time.time > timeLastFire + fireRate)
            {
                fire();
                timeLastFire = Time.time;
                //flip();
            }

        }


    }

    
   
    


    void OnTriggerStay2D(Collider2D d)
    {
        Debug.Log("something has been detected");
        if (d.tag == "Player")
        {
            if (d.tag == "Player")
            {
                Debug.Log("herro!");
                if (target.transform.position.x < transform.position.x)
                {

                    playerLeft = true;
                }

                if (target.transform.position.x > transform.position.x)
                {
                    playerLeft = false;
                    // flip();
                }
                //detectPlayer = true;
                // target = d.tag;
            }





            Debug.Log("player has been detected");
            detectPlayer = true;
        }
    }






    void OnTriggerExit2D(Collider2D d)
    {
        if (d.tag == "Player")
        {
            Debug.Log("player has evaded detection");
            detectPlayer = false;
        }

    }








    void fire()
    {
        // Creates Projectile and add its to the Scene
        // - projectPrefab is the thing to create
        // - projectileSpawnPoint is where and what rotation to use when created

        projectileBullet temp;
        Debug.Log("Fire!");

        

        if (playerLeft)
        {
            Debug.Log("player is on the left!");
            
            temp =
            Instantiate(projectileBulletPrefab, projectileSpawnLeft.position, projectileSpawnLeft.rotation);
            

            temp.speed = -projectileForce;
        }

        else
        {
            Debug.Log("player is on the right!");
            temp =
            Instantiate(projectileBulletPrefab, projectileSpawnRight.position, projectileSpawnRight.rotation);

            temp.speed = projectileForce;
        }


        temp.tag = "enemyProjectile";
    }





}
