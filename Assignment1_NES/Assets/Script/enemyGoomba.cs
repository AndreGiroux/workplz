﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGoomba : MonoBehaviour {

    Rigidbody2D rb;

    public float speed;

    //public float 

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D found.");

            rb = gameObject.AddComponent < Rigidbody2D>();
        }

        if (speed <= 0)
        {
            speed = 2.0f;
        }



	}
	
	// Update is called once per frame
	void Update () {

        rb.velocity = new Vector2(speed, rb.velocity.y);

    }



    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag == "enemyBarrier")
        {
            speed = -speed;
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            speed = -speed;
        }
    }


}
