﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// new
using UnityEngine.SceneManagement;


public class playerScript : MonoBehaviour {



    Rigidbody2D rb; // create rigid body

    public float speed; // speed variable

    public float jumpForce; // jumpforce variable

    //power up
    public float powerUpJump;

    public float powerUpTimer;

 //////////////////////////////////////////

    // week 4 added
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public float groundCheckRadius;


    //public bool fell = false;

        //spawn location
    public int spawnLocation;


    //////////////////////////////////////////
    Animator anim;
    // mario flip
    public bool isFacingLeft;


	// Use this for initialization
	void Start () {


        if (powerUpJump <= 0)
        {
            powerUpJump = 6.5f;
            Debug.Log("powerUpForce is not valide, defaulting to " + powerUpJump);
        }

        // spanw
        if (spawnLocation < 0)
        {
            spawnLocation = 0;
        }





        // powerUp Timer
        if (powerUpTimer <= 0)
        {
            // Assign a default value if one is not set in the Inspector
            powerUpTimer = 7.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log(" jumpPowerUpTimer was not set. Defaulting to " + powerUpTimer);
        }






        rb = GetComponent<Rigidbody2D>(); // grab rigid body

       anim = GetComponent<Animator>();

        

        if (speed <= 0)
        {
            speed = 5.0f;
            Debug.LogWarning("speed not set. speed now set to " + speed); // if spped is not set make speed = to 5;
             
        }



        if (!rb)
        {
            Debug.LogError("No rigid body attached to " + name); // if rigid body is not attached to player
        }







        if (!anim)
        {
            Debug.LogError("No Animator attached to " + name); // if animator isnt attached;
        }



        if (jumpForce <= 0)
        {


            jumpForce = 15.0f;

            Debug.LogWarning("jumpForce not set. Defaulting to " + jumpForce); // jumpForce = or below to 0 then set it to 5
        }


        //// ground checks

        if (!groundCheck)
        {
            Debug.LogError("No groundCheck attached to " + name); // if rigidbody isnt attached;
        }

        
        if (groundCheckRadius <= 0)
        {


            groundCheckRadius = 0.22f;

            Debug.LogWarning("GroundCheckRadiues not set. Defaulting to " + groundCheckRadius); // jumpForce = or below to 0 then set it to 5
        }
        
    }

    // Update is called once per frame
    void Update () {



        // check to see if its grounded
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);



        float moveValue = Input.GetAxisRaw("Horizontal"); // connects moveValue to horizontal axis input

        //bool attackValue = Input.GetButtonDown("Fire1");

       // bool runValue = Input.GetButtonDown("Fire3");

       //bool jumpValue = Input.GetButtonDown("Jump");




        // jump

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("Jump");

            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

           

        }

        /*
        if (Input.GetButton("Jump"))
        {
            anim.SetBool("JumpValue", jumpValue);
        }
        */

        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y); // mpve moveValue * x and don't do anything to y. 




        // animations
        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));

        anim.SetBool("Grounded", isGrounded);

        anim.SetFloat("Speed", speed);

        if (Input.GetButton("Fire1"))
        {
            Debug.Log(speed);
            speed = 8.5f;
        }
        
        else
        {
            speed = 5.0f;
        }









        if ((!isFacingLeft && moveValue < 0) || (isFacingLeft && moveValue > 0))
            flip();




        //if (fell == true)
        //
            //Destroy(gameObject);
        //}


    }






   

        
   



    void flip()
    {
        isFacingLeft = !isFacingLeft;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x = -scaleFactor.x;

        transform.localScale = scaleFactor;
    }


    void OnCollisionEnter2D(Collision2D c)
    {

        GameObject manager = GameObject.Find("GameManager");
        GameManager managerScript = manager.GetComponent<GameManager>();


        //Debug.Log("Ded");

        if (c.gameObject.tag == "enemy")
        {
            Debug.Log("Enemy touched the player");

            managerScript.lives = managerScript.lives - 1;


            Destroy(gameObject);

            SceneManager.LoadScene("Level1");


        }
        else
        {
            Debug.Log("Exception");
        }


    }

    void OnTriggerEnter2D(Collider2D c)
    {


        GameObject manager = GameObject.Find("GameManager");
        GameManager managerScript = manager.GetComponent<GameManager>();
        if (c.gameObject.tag == "killzone")
        {


            managerScript.lives = managerScript.lives - 1;

            Destroy(gameObject);
            //Debug.Log("Ded");

            SceneManager.LoadScene("Level1");


        }

        // power up place holder
        if(c.gameObject.tag == "powerUp")
        {
            Destroy(c.gameObject);
            jumpForce += powerUpJump;
            StartCoroutine("stopPowerUp");
        }


        if (c.gameObject.tag == "enemyProjectile")
        {


            managerScript.lives = managerScript.lives - 1;

            Destroy(gameObject);
            //Debug.Log("Ded");

            SceneManager.LoadScene("Level1");


        }


        else
        {
            Debug.Log("Ecept");
        }
    }


    IEnumerator stopPowerUp()
    {
        yield return new WaitForSeconds(powerUpTimer);
        jumpForce -= powerUpJump;
    }


}
