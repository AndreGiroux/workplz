﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileBullet : MonoBehaviour {

    // Used to tell GameObject (Projectile) how fast to move
    public float speed;

    // Used to tell GameObject (Projectile) how long to live without colliding with anything
    public float lifeTime;


    // Use this for initialization
    void Start () {

        // Check if speed was set to something not 0
        if (speed == 0)
        {
            
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("speed was not set. Defaulting to " + speed);
        }

        if (speed > 0)
        {
            Vector3 scaleFactor = transform.localScale;

            // Flip scale of 'x' variable
            scaleFactor.x *= -1;    // scaleFactor.x = -scaleFactor.x;

            // Update scale to new flipped value
            transform.localScale = scaleFactor;
        }

        // Check if speed was set to something not 0
        if (lifeTime <= 0)
        {
            // Assign a default value if one is not set in the Inspector
            lifeTime = 1.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("lifeTime was not set. Defaulting to " + lifeTime);
        }

        // Take Rigidbody2D component and change its velocity to value passed
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);

        
        // Destroy gameObject after 'lifeTime' seconds
        Destroy(gameObject, lifeTime);



    }
	
	// Update is called once per frame
	void Update () {
		
	}




    void OnTriggerStay2D(Collider2D d)
    {
        if (d.tag == "Player")
        {
            Destroy(gameObject);
            Debug.Log("Player has been hit!!!");

        }


        

    }


}
